<?php

namespace classes;
/**
 * Que sont les espaces de noms ? Dans leur définition la plus large, ils représentent un moyen d'encapsuler des éléments.
 * Cela peut être conçu comme un concept abstrait, pour plusieurs raisons. Par exemple, dans un système de fichiers,
 * les dossiers représentent un groupe de fichiers associés et servent d'espace de noms pour les fichiers qu'ils contiennent.
 * Un exemple concret est que le fichier foo.txt peut exister dans les deux dossiers /home/greg et /home/other,
 * mais que les deux copies de foo.txt ne peuvent pas co-exister dans le même dossier.
 * De plus, pour accéder au fichier foo.txt depuis l'extérieur du dossier /home/greg,
 * il faut préciser le nom du dossier en utilisant un séparateur de dossier, tel que /home/greg/foo.txt.
 * Le même principe s'applique aux espaces de noms dans le monde de la programmation.
 *
 * Dans le monde PHP,
 * les espaces de noms sont conçus pour résoudre deux problèmes que rencontrent les auteurs de bibliothèques et d'applications lors de la réutilisation d'éléments tels que des classes ou des bibliothèques de fonctions :
 * - Collisions de noms entre le code que vous créez, les classes, fonctions ou constantes internes de PHP, ou celle de bibliothèques tierces.
 * - La capacité de faire des alias ou de raccourcir des Noms_Extremement_Long pour aider à la résolution du premier problème, et améliorer la lisibilité du code.
 *
 * Note: Les noms d'espaces de noms ne sont pas sensible à la casse.
 * Note: Les espaces de noms PHP (PHP\...) sont réservés pour l'utilisation interne du langage.
 */

/**
 * La class character est abstraite (abstract) afin de ne pas permettre son instanciation et forcer l'instanciation des classes héritées (dans cet exemple les classes pc et npc).
 * Si vous tentez d'instancier une classe abstraite, une erreur sera renvoyée.
 */
abstract class character
{
    public string $name = 'character';
    private int $id;
    private int $hp;
    private int $attack;
    private int $defense;
    private weapon $weapon;
    private magic $magic;

    /**
     * @param string $name
     * @param int $id
     */
    public function __construct(string $name, int $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    /**
     * GETTERS
     */

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getHp(): int
    {
        return $this->hp;
    }

    public function getAttack(): int
    {
        return $this->attack;
    }

    public function getDefense(): int
    {
        return $this->defense;
    }

    public function getWeapon(): object
    {
        return $this->weapon;
    }

    public function getMagic(): object
    {
        return $this->magic;
    }

    /**
     * SETTERS
     */

    /**
     * @param int $id
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setHp(int $hp): void
    {
        $this->hp = $hp;
    }

    public function setAttack(int $attack): void
    {
        $this->attack = $attack;
    }

    public function setDefense(int $defense): void
    {
        $this->defense = $defense;
    }

    public function setWeapon(weapon $weapon): void
    {
        $this->weapon = $weapon;
    }

    public function giveWeapon(array $weapons): void
    {
        $this->weapon = $weapons[rand(0, count($weapons) -1)];
    }

    public function giveMagic(array $magic): void
    {
        $this->magic = $magic[array_rand($magic)];
    }

    /**
     * @param object $target
     * @return string
     */
    public function Attack(object $target): string
    {
        $damage = 0;
        $def = 0;
        $parry = false;
        $dodge = false;

        if ($this->getMagic()->category == \classes\capacity::CAT_OFFENSIVE) {
            //magic attack
            $damage = $this->getMagic()->getDamage();
        } elseif ($this->getWeapon()->category == \classes\capacity::CAT_OFFENSIVE) {
            //weapon attack
            $damage = $this->getWeapon()->getDamage();
            $def = $target->getDefense();
            // parry ?
            if ($this->getWeapon()->type == $this->getWeapon()::CAT_MELEE && $target->getWeapon()->category == \classes\capacity::CAT_DEFENSIVE) {
                $parry = $this->parry();
            }
            // dodge ?
            if ($target->getWeapon()->type == $target->getWeapon()::CAT_DEFENSIVE) {
                $dodge = $this->dodge();
            }
            // defensive magic bonus
            if ($target->getMagic()->category == \classes\capacity::CAT_DEFENSIVE) {
                $def += $target->getMagic()->getDefense();
            }
        } else {
            // cancel attack
            return $this->name . ' échoue dans son attaque contre ' . $target->name . ' car il ne possède pas de capacité offensive !<br>';
        }

        // ça touche ?
        $result = rand(1, $this->attack) - rand(1, $def);

        if ($result > 0 && $damage) {
            // L'attaque réussit

            // l'attaque a été parée ou esquivée ?
            if ($parry) {
                $string = 'parée par ' . $target->name;
            } elseif ($dodge) {
                $string = 'esquivée par ' . $target->name;
            } else {
                $live = $target->getHp() - $damage;
                $string = 'touché! ' . $target->name . ' est ';
                if ($live > 0) {
                    $string .= 'blessé par ' . $this->getWeapon()->name . ' qui lui inflige ' . $damage . ' points de dégâts! Il lui reste ' . $live . ' points de vie';
                } else {
                    $live = 0;
                    $string .= 'mort!';
                }
                $target->setHp($live);
            }
        } else {
            // ça touche pas
            $string = 'raté';
        }

        return $this->name . ' attaque ' . $target->name . ' avec une attaque de ' . $this->attack . ' face à une défense de ' . $def . '<br> Résultat : ' . $result . ' : ' . $string . '<br>';
    }

    public function parry(): bool {

        $attack = $this->getAttack();

        // Quelle tranche dans getAttack()?
        $attackRange = floor($attack / 10); // Obtient la tranche de 10 (ex. 23 / 10 = 2)

        // Pourcentage de chance de parer en fonction de la tranche
        $parryChance = ($attackRange + 1) * 10; // Ajoute 1 pour obtenir le pourcentage (ex. tranche 2 => 20%)

        // Nombre aléatoire entre 1 et 100 pour déterminer si la parade réussit
        $randomNumber = rand(1, 100);

        // Si le nombre aléatoire est inférieur ou égal à la chance de parade, la parade réussit
        if ($randomNumber <= $parryChance) {
            return true;
        }

        return false;
    }

    public function dodge(): bool {

        $targetDefense = $this->getDefense();

        // Quelle tranche dans getDefense()?
        $defenseRange = floor($targetDefense / 10); // Obtient la tranche de 10 (ex. 25 / 10 = 2)

        // Pourcentage de chance d'esquiver en fonction de la tranche
        $dodgeChance = ($defenseRange + 1) * 10; // Ajoute 1 pour obtenir le pourcentage (ex. tranche 2 => 20%)

        // Nombre aléatoire entre 1 et 100 pour déterminer si l'esquive réussit
        $randomNumber = rand(1, 100);

        // Si le nombre aléatoire est inférieur ou égal à la chance d'esquiver, l'esquive réussit
        if ($randomNumber <= $dodgeChance) {
            return true;
        }

        return false;
    }
}